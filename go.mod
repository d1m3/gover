module gitlab.com/dime.nicius/app

go 1.12

require (
	github.com/gorilla/mux v1.7.4
	github.com/xanzy/go-gitlab v0.31.0
)
